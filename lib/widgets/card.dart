import 'package:flutter/material.dart';

class HomeCard extends StatelessWidget {
  const HomeCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    const customTextStyle = TextStyle(
      color: Colors.white,
      fontFamily: 'Serif',
      fontSize: 18,
      fontWeight: FontWeight.bold,
    );

    return Container(
      width: width * .5,
      height: 100,
      color: Color(0xff219ebc),
      child: const Row(
        children: [
          Column(
            children: [
              Text(
                "Mobile Dsign",
                style: customTextStyle,
              ),
              Text(
                "Client project",
                style: customTextStyle,
              ),
              Text(
                "price",
                style: customTextStyle,
              )
            ],
          ),
          Text(
            "Menu",
          )
        ],
      ),
    );
  }
}
