import 'package:get/get.dart';
import 'package:shoping/screens/home_screen.dart';

appRoutes() => [
      GetPage(
        name: '/',
        page: () => HomeScreen(),
        transition: Transition.leftToRightWithFade,
        transitionDuration: const Duration(milliseconds: 500),
      ),
    ];

class MyMiddelware extends GetMiddleware {
  @override
  GetPage? onPageCalled(GetPage? page) {
    print(page?.name);
    return super.onPageCalled(page);
  }
}
