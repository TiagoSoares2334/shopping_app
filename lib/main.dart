import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shoping/routes.dart';
import 'package:shoping/screens/home_screen.dart';

void main() {
  runApp(GetMaterialApp(
    title: "SHOPPING APP",
    initialRoute: '/',
    getPages: appRoutes(),
  ));
}
